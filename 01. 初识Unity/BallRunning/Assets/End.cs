using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : MonoBehaviour
{
    GameObject endUI;
    void Start()
    {
        endUI = GameObject.Find("GameEnd");
        Debug.Log("endUI is " + endUI);
        endUI.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.name == "Player")
        {
            Debug.Log("����");
            endUI.SetActive(true);

            Time.timeScale = 0;
        }
    }
}
