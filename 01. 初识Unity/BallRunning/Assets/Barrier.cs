using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    GameObject overUI;
    void Start()
    {
        overUI = GameObject.Find("GameOver");
        Debug.Log("overUI is " + overUI);
        overUI.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            Debug.Log("��ײ");
            overUI.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
