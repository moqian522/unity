using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(10, 0, 0);
    }

    private void OnTriggerEnter(Collider collider) 
    {
        Debug.Log(collider.name + " 刚碰到了我");
        Destroy(gameObject);
    }

    private void OnTriggerStay(Collider collider) 
    {
        Debug.Log(collider.name + " 还在碰到了我");
    }

    private void OnTriggerExit(Collider collider) 
    {
        Debug.Log(collider.name + " 不在碰到了我");
    }

    
   
}
